<?php

include('login.php');

$db = new PDO("mysql:host=localhost;dbname=weblog","root", "");

$login_session = $_SESSION['login_user'];
$day = $_POST['day'];
$month = $_POST['month'];
$year = $_POST['year'];

$date = $year.'-'.$month.'-'.$day;

$query_id = "SELECT id FROM `membre` WHERE pseudo = '$login_session'";
$result_id = $db->query($query_id);
$donnees_id = $result_id->fetch(); 
$id = $donnees_id['id'];

$query_date = "UPDATE `membre` SET `birthday` = '$date' WHERE `membre`.`id` = $id";
$result_date = $db->query($query_date);
$donnees_date = $result_date->fetch(); 

?>
<!DOCTYPE html>
<head>
 <meta charset="utf-8">
 <title>El Blog del UNICORN</title>
 <link href="https://fonts.googleapis.com/css?family=Just+Another+Hand|Lato" rel="stylesheet">
 <link rel="stylesheet" href="style.css">
</head>
<body role="document">
 <header id="header">
  <?php
  if (isset($_SESSION['login_user'])) {
   include('session_start.php');
  } else { 
   include('authentification.php');
  }
  ?>
  <div class="bandeau">
   <h1 id="title" role="banner">• • Blog del Unicorn • •</h1>
  </div>
  <nav id="blognav">
   <ul>
    <li class="menu"><a href="Index.php">Accueil</a></li>
    <li class="menu"><a href="pagemembre.php">Espace Membre</a></li>
    <li class="menu"><a href="contact.php">Contact</a></li>
   </ul>
  </nav>
 </header>
 <div id="main" role="main">
  <div id="membre-container">
   <?php
   if (isset($_SESSION['login_user'])) {
    include('pagemembre_edit.php');
   } else { 
    echo "Vous devez être connecté pour accèder à cette page !";
   }
   ?>
  </div>
 </div>
 <footer id="footer" role="contentinfo"><a href="adminlog.php">Panneau d'administration</a></footer>
</body>
</html>