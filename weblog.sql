-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 15 Janvier 2017 à 18:47
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `weblog`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pwd` int(255) NOT NULL,
  `mail` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admins`
--

INSERT INTO `admins` (`id`, `login`, `pwd`, `mail`) VALUES
(1, 'toto', 1234, 'toto@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `billet`
--

CREATE TABLE `billet` (
  `id_billet` int(11) NOT NULL,
  `date_billet` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `tags` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `billet`
--

INSERT INTO `billet` (`id_billet`, `date_billet`, `title`, `content`, `tags`) VALUES
(3, '2017-01-14', 'SARAPORTEUNMAX', 'MWA KAN JE SERAI GRANDE JE SERAI DEVELLOPEUSE ET JE ME FERAI UN MAX DE BIFF MDR !!', 'TROLOL ARGENT BIFF'),
(14, '2017-01-14', 'LOL', 'TEST', 'TEST');

-- --------------------------------------------------------

--
-- Structure de la table `billet_comment`
--

CREATE TABLE `billet_comment` (
  `id_billet` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id_comment` int(11) NOT NULL,
  `date_comment` datetime NOT NULL,
  `content_comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `user_id` int(11) NOT NULL DEFAULT '2',
  `id` int(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `passe` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` date DEFAULT NULL,
  `genre` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `membre`
--

INSERT INTO `membre` (`user_id`, `id`, `pseudo`, `passe`, `email`, `birthday`, `genre`) VALUES
(2, 3, 'anthony', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'anthony@gmail.com', NULL, ''),
(2, 4, 'sunshany', 'f147fc3f404aed6cfe15687c0b96661594a601de', 'oppa.pony.style@gmail.com', NULL, ''),
(2, 11, 'Sarah', '20e65975dbaeaf0bc3bf0f674f19631f09b0444f', 'sarah.amairi@epitech.eu', NULL, ''),
(2, 36, 'Taymi', '81527347ba0b41b24b9c6d083a6bf9ab2807be69', 'soleneeeb@gmail.com', NULL, ''),
(2, 37, 'Truc', '89fc2e196c3516c525c3672527317c2767789325', 'lol@gmail.com', NULL, ''),
(1, 38, 'Admin', '2167e74daf260e9ff71edd265dd4239d43528b7e', 'pinkiepie@gmail.com', '2002-04-16', ''),
(2, 39, 'Testnumero', '2167e74daf260e9ff71edd265dd4239d43528b7e', 'pinkie@gmail.com', '1997-04-19', 'Homme');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `billet`
--
ALTER TABLE `billet`
  ADD PRIMARY KEY (`id_billet`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `billet`
--
ALTER TABLE `billet`
  MODIFY `id_billet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
