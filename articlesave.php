<?php 
$bdd = new PDO("mysql:host=localhost;dbname=weblog","root","");

$titre = htmlspecialchars($_POST['titrepost']);
$contenu = htmlspecialchars($_POST['contenupost']);
$tags = htmlspecialchars($_POST['tagspost']);
$query = "SELECT CURRENT_DATE";
$result = $bdd->query($query);
$donnees_CD = $result->fetch();
$CD = $donnees_CD['CURRENT_DATE'];
$insertmbr = $bdd->prepare("INSERT INTO billet(date_billet,title, content, tags) VALUES(?, ?, ?, ?)");
$insertmbr->execute(array($CD, $titre, $contenu, $tags));
header('Location: index.php');  
?>