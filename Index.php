<?php
include('login.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>El Blog del UNICORN</title>
	<link href="https://fonts.googleapis.com/css?family=Just+Another+Hand|Lato" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
</head>
<body role="document">
	<header id="header">
		<?php
		if (isset($_SESSION['login_user'])) {
			include('session_start.php');
		} else { 
			include('authentification.php');
		}
		?>
		<div class="bandeau">
			<h1 id="title" role="banner">• • Blog del Unicorn • •</h1>
		</div>
		<nav id="blognav">
			<ul>
				<li class="menu"><a href="Index.php">Accueil</a></li>
				<li class="menu"><a href="pagemembre.php">Espace Membre</a></li>
				<li class="menu"><a href="contact.php">Contact</a></li>
			</ul>
		</nav>
	</header>
	<div id="main" role="main">
		<?php
		$bdd = new PDO("mysql:host=localhost;dbname=weblog","root","");
		$query = "SELECT * FROM billet ORDER BY id_billet DESC";
		$result = $bdd->query($query);

		while($billet = $result->fetch()) {

			echo '<article role="article">
				<header class="article-header">
					<h2 class="titrebillet">'. $billet['title'].'</h2>
					<p class="datebillet"> le '. $billet['date_billet'].'</p>
				</header>
				<div class="content">'. $billet['content'].'
				</div>
				<footer class="article-footer">Voir les commentaires/Ajouter un commentaire</footer>
			</article>';

		};
		?>
		
	</div>
	<footer id="footer" role="contentinfo"><a href="adminlog.php">Panneau d'administration</a></footer>
	<script type="text/javascript" src="js/destroy_session.js"></script>
</body>
</html>