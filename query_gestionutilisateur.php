<?php

include('login.php');

$db = new PDO("mysql:host=localhost;dbname=weblog","root", "");

$login_session = $_SESSION['login_user'];
$login = $_POST['pseudo'];
$pwd = $_POST['password'];
$mail = $_POST['mail'];

$query_password = "SELECT passe FROM `membre` WHERE pseudo = '$login_session'";
$result_password = $db->query($query_password);
$donnees_password = $result_password->fetch(); 
$password = $donnees_password['passe'];

$query_id = "SELECT id FROM `membre` WHERE pseudo = '$login_session'";
$result_id = $db->query($query_id);
$donnees_id = $result_id->fetch(); 
$id = $donnees_id['id'];
$pwd = sha1($pwd);

if (!empty($login) && !empty($pwd) && !empty($mail)) {
 $query_new_pseudo = "UPDATE `membre` SET pseudo ='$login' WHERE id = '$id'";
 $result_pseudo = $db->query($query_new_pseudo);
 $donnees_pseudo = $result_pseudo->fetch();

 $query_pwd = "UPDATE `membre` SET passe = '$pwd' WHERE id = '$id'";
 $result_newpwd = $db->query($query_pwd);
 $donnees_newpwd = $result_newpwd->fetch();

 $query_newmail = "UPDATE `membre` SET email = '$mail' WHERE id = '$id'";
 $result_newmail = $db->query($query_newmail);
 $donnees_newmail = $result_newmail->fetch();

 Session_destroy();
 echo '<br><br><div class="alert alert-dismissable alert-success">
  <p>Votre modification a été prise en compte, veuillez vous reconnecter avec vos nouveaux identifiants. <meta http-equiv="refresh" content="4; URL=index.php"></p>
  </div>';
}

?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title>El Blog del UNICORN</title>
 <link href="https://fonts.googleapis.com/css?family=Just+Another+Hand|Lato" rel="stylesheet">
 <link rel="stylesheet" href="style.css">
</head>
<body role="document">
 <header id="header">
  <?php
  if (isset($_SESSION['login_user'])) {
   include('session_start.php');
  } else { 
   include('authentification.php');
  }
  ?>
  <div class="bandeau">
   <h1 id="title" role="banner">• • Blog del Unicorn • •</h1>
  </div>
  <nav id="blognav">
   <ul>
    <li class="menu"><a href="Index.php">Accueil</a></li>
    <li class="menu"><a href="pagemembre.php">Espace Membre</a></li>
    <li class="menu"><a href="contact.php">Contact</a></li>
   </ul>
  </nav>
 </header>
 <div id="main" role="main">
  <div id="membre-container">
   <?php
   if (isset($_SESSION['login_user'])) {
    include('pagemembre_edit.php');
   } else { 
    echo "Vous devez être connecté pour accèder à cette page !";
   }
   ?>
  </div>
 </div>
 <footer id="footer" role="contentinfo"><a href="adminlog.php">Panneau d'administration</a></footer>
</body>
</html>