<?php
include('login.php');
?>
<!doctype html>
<head>
	<meta charset="utf-8">
	<title>El Blog del UNICORN</title>
	<link href="https://fonts.googleapis.com/css?family=Just+Another+Hand|Lato" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
</head>
<body role="document">
	<header id="header">
		<?php
		if (isset($_SESSION['login_user'])) {
			include('session_start.php');
		} else { 
			include('authentification.php');
		}
		?>	
		<div class="bandeau">
			<h1 id="title" role="banner">• • Blog del Unicorn • •</h1>
		</div>
		<nav id="blognav">
			<ul>
				<li class="menu"><a href="Index.php">Accueil</a></li>
				<li class="menu"><a href="pagemembre.php">Espace Membre</a></li>
				<li class="menu"><a href="contact.php">Contact</a></li>
			</ul>
		</nav>
	</header>
	<div id="main" role="main">
		<div id="formcontainer">
			<h2 id="identadmin">IDENTIFICATION ADMINISTRATEUR</h2>
			<?php 
			$bdd = new PDO("mysql:host=localhost;dbname=weblog","root","");
			$login = $_SESSION['login_user'];

			$query_id = "SELECT user_id FROM `membre` WHERE pseudo = '$login'";
			$result_id = $bdd->query($query_id);
			$donnees_id = $result_id->fetch(); 
			$id = $donnees_id['user_id'];

			if ($id == '2') {
				echo "<p>Vous n'êtes pas administrateur :S</p>";
			} else {
				echo '<form method="post" action="panneauadmin_check.php">
				<input class="inputadmin" type="text" placeholder=" Username" name="pseudo" required>
				<input class="inputadmin" type="password" placeholder=" Password" name="psw" required>
				<button id="submitadmin" type="submit">Login</button>
			</form>';
			}
			?>
		</div>
	</div>
	<footer id="footer" role="contentinfo"><a href="adminlog.php">Panneau d'administration</a></footer>
</body>
</html>